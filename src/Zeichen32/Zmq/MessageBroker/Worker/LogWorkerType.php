<?php
/**
 * Created by PhpStorm.
 * User: jens
 * Date: 30.09.13
 * Time: 17:06
 */

namespace Zeichen32\Zmq\MessageBroker\Worker;

use Zeichen32\Zmq\MessageBroker\Job\JobInterface;
use Zeichen32\Zmq\MessageBroker\MessageBroker;

class LogWorkerType extends AbstractWorker {
    /**
     * Return Worker type
     *
     * @return string
     */
    public function getType()
    {
        return 'LOG_WORKER_TYPE';
    }

    /**
     * Check if a job is supported by worker
     *
     * @param JobInterface $job
     * @return bool
     */
    public function support(JobInterface $job)
    {
        return false;
    }

    /**
     * Get all posible commands for worker.
     *
     * array('HELLOWORLD' => 'onHelloWorldCommand')
     *
     * @return array
     */
    public function getCommands()
    {
        return array_merge(parent::getCommands(), array(
            'LOG' => 'onLogComplete',
        ));
    }

    public function onLogComplete($command, \ZMQSocket $socket) {

        $this->setNotWorking();
        return true;
    }

    public function sendLogs(\ZMQSocket $socket, array $logs)
    {
        $this->setWorking();

        $socket->send($this->getWorkerAddress(), \ZMQ::MODE_SNDMORE);
        $socket->send("", \ZMQ::MODE_SNDMORE);
        $socket->send('LOG', \ZMQ::MODE_SNDMORE);
        $socket->send("", \ZMQ::MODE_SNDMORE);
        $socket->send(json_encode($logs));

        return true;
    }
}
