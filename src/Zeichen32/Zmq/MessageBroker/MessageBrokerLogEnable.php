<?php
/**
 * Created by PhpStorm.
 * User: jens
 * Date: 30.09.13
 * Time: 17:19
 */

namespace Zeichen32\Zmq\MessageBroker;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Zeichen32\Zmq\MessageBroker\Worker\LogWorkerType;
use ZMQSocket;

class MessageBrokerLogEnable extends MessageBroker {

    protected $logQueue = array();

    function __construct(ZMQSocket $frontendSocket, ZMQSocket $backendSocket, array $options = array())
    {
        parent::__construct($frontendSocket, $backendSocket, $options);

        $this->addWorkerType('LOG_WORKER_TYPE', function($workerAddress) {
           return new LogWorkerType($workerAddress);
        });
    }


    public function log($message, $type = 'debug')
    {
        parent::log($message, $type);

        if(NULL !== $this->hasLogWorker()) {
            $this->logQueue[] = array(
                'time' => time(),
                'type' => $type,
                'msg' => $message,
            );
        }
    }

    protected function hasLogWorker() {
        /**
         * @var $worker \Zeichen32\Zmq\MessageBroker\Worker\WorkerInterface
         */
        foreach ($this->availableWorker as $worker) {
            if($worker->getType() == 'LOG_WORKER_TYPE') {
                return true;
            }
        }

        return null;
    }

    protected function afterCall()
    {
         /**
         * @var $worker \Zeichen32\Zmq\MessageBroker\Worker\LogWorkerType
         */
        if(count($this->logQueue) > 0 && count($this->availableWorker) > 0) {

            $clear = false;
            foreach($this->availableWorker as $worker) {

                if($worker->getType() == 'LOG_WORKER_TYPE' && !$worker->isWorking() && !$worker->isWaitingForNextPing() && !$worker->hasCloseRequest()) {
                    $worker->sendLogs($this->backendSocket, $this->logQueue);
                    $clear = true;
                }
            }

            if($clear === true || count($this->logQueue) > 1000) {
                $this->logQueue = array();
            }
        }

        parent::afterCall();
    }

} 