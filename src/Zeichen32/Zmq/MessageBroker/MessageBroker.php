<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * Developer: Jens Averkamp
 * Date: 08.08.13
 * Time: 09:26
 */

namespace Zeichen32\Zmq\MessageBroker;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Zeichen32\Zmq\LoggableInterface;
use Zeichen32\Zmq\Logger\LoggerInterface;
use Zeichen32\Zmq\MessageBroker\Job\JobFactoryInterface;
use Zeichen32\Zmq\MessageBroker\Job\JobInterface;
use Zeichen32\Zmq\MessageBroker\Worker\WorkerFactoryInterface;
use Zeichen32\Zmq\MessageBroker\Worker\WorkerInterface;
use ZMQContext;
use ZMQSocket;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageBroker implements LoggableInterface
{

    const WORKER_COMMAND_SUBSCRIBE = 'SUBSCRIBE';
    const WORKER_COMMAND_UNSUBSCRIBE = 'UNSUBSCRIBE';
    const WORKER_COMMAND_SHUTDOWN = 'SHUTDOWN';
    const WORKER_COMMAND_PING = 'PING';
    const WORKER_COMMAND_PONG = 'PONG';
    const WOKRER_COMMAND_HANDLEJOB = 'HANDLEJOB';
    const WORKER_COMMAND_STATUS = 'STATUS';

    const WORKER_RESPONSE_JOB_COMPLETE = 'JOBCOMPLETE';
    const WORKER_RESPONSE_JOB_FAILED = 'JOBFAILED';
    const WORKER_RESPONSE_JOB_SUCCESS = 'JOBSUCCESS';
    const WORKER_RESPONSE_STATUS_UPDATE = 'WORKERSTATUS';

    protected $options;

    protected $frontendSocket;
    protected $backendSocket;

    protected $availableWorker = array();

    protected $workerTypes = array();
    protected $jobTypes = array();

    protected $jobQueue = array();

    protected $logger;

    protected $connected = false;

    protected $lastStatus;

    function __construct(ZMQSocket $frontendSocket, ZMQSocket $backendSocket, array $options = array())
    {

        $resolver = new OptionsResolver();
        $this->setDefaultOptions($resolver);
        $this->options = $resolver->resolve($options);

        $this->frontendSocket = $frontendSocket;
        $this->backendSocket = $backendSocket;

        $this->lastStatus = new \DateTime();

        $this->logger = function ($message, $type) {
        };
    }

    public function setLogger($logger)
    {
        if (is_callable($logger) || $logger instanceof LoggerInterface) {
            $this->logger = $logger;
        }
    }

    public function log($message, $type = 'debug')
    {
        if (is_callable($this->logger)) {
            call_user_func_array($this->logger, array($message, $type));
        }
    }

    protected function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'ipAddressFrontend',
            'ipAddressBackend',
        ));

        $resolver->setOptional(array(
            'portFrontend',
            'portBackend',
        ));

        $resolver->setDefaults(array(
            'portFrontend' => 4444,
            'portBackend' => 5555,
            'timeout' => 3000,
        ));

        $resolver->setAllowedTypes(array(
            'portFrontend' => 'integer',
            'portBackend' => 'integer',
            'timeout' => 'integer',
        ));
    }

    protected function getServerAddress($type = 'frontend')
    {
        if ($type == 'frontend') {
            return sprintf('tcp://%s:%d', $this->options['ipAddressFrontend'], $this->options['portFrontend']);
        } elseif ($type == 'backend') {
            return sprintf('tcp://%s:%d', $this->options['ipAddressBackend'], $this->options['portBackend']);
        }

        throw new \InvalidArgumentException('Type can be only "frontend" or "backend"');
    }

    public function addWorkerTypeFactory(WorkerFactoryInterface $factory) {
        $this->workerTypes[$factory::getType()] = $factory;
    }

    public function addJobTypeFactory(JobFactoryInterface $factory) {
        $this->jobTypes[$factory::getType()] = $factory;
    }

    public function addWorkerType($workerType, $closure)
    {
        if (is_callable($closure)) {
            $this->workerTypes[$workerType] = $closure;
            return;
        }

        throw new \InvalidArgumentException('Argument closure must be callable');
    }

    public function addJobType($jobType, $closure)
    {
        if (is_callable($closure)) {
            $this->jobTypes[$jobType] = $closure;
            return;
        }

        throw new \InvalidArgumentException('Argument closure must be callable');
    }

    public function run()
    {

        $writable = array();
        $readable = array();

        $this->frontendSocket->bind($this->getServerAddress('frontend'));
        $this->backendSocket->bind($this->getServerAddress('backend'));

        $this->log('Server startet...');

        $this->connected = true;

        while (true) {
            $poll = new \ZMQPoll();

            $poll->add($this->frontendSocket, \ZMQ::POLL_IN);
            $poll->add($this->backendSocket, \ZMQ::POLL_IN);

            $events = $poll->poll($readable, $writable, $this->options['timeout']);

            if ($events > 0) {

                foreach ($readable as $socket) {

                    if ($socket === $this->backendSocket) {

                        // Before Call Event
                        $this->beforeCall($socket);

                        // Backend Event
                        if (!$this->onBackendCall($socket)) {
                            $this->log('Command has faild', 'error');
                        }

                    } elseif ($socket === $this->frontendSocket) {

                        // Before Call Event
                        $this->beforeCall($socket);

                        // Frontend Event
                        $this->onFrontendCall($socket);

                    }
                }
            }

            // After Event
            $this->afterCall();
        }
    }

    /**
     * Handle the backend part
     *
     * @param ZMQSocket $socket
     * @return bool
     */
    protected function onBackendCall(ZMQSocket $socket)
    {

        //  Queue worker address for LRU routing
        $workerAddress = $socket->recv();

        if (array_key_exists($workerAddress, $this->availableWorker)) {
            $worker = $this->availableWorker[$workerAddress];
        } else {
            $worker = null;
        }

        $this->emptyFrame($socket);

        $command = $socket->recv();

        // Subscribe a new worker
        if (is_null($worker)) {

            if ($command != self::WORKER_COMMAND_SUBSCRIBE) {
                return false;
            }

            try {
                return $this->onSubscribeWorker($socket, $workerAddress);
            } catch (\Exception $exp) {
                return false;
            }
        }

        if (!$worker instanceof WorkerInterface) {
            return false;
        }

        // Handle commands
        switch ($command) {

            // Handle the unsubscribe command
            case self::WORKER_COMMAND_UNSUBSCRIBE:
                return $this->onUnsubscribeWorker($socket, $worker);

            // Handle the receive pong command
            case self::WORKER_COMMAND_PONG:
                return $this->onPong($socket, $worker);

            // Handle worker specific command
            default:
                if (!$this->onCustomWorkerCommand($command, $worker, $socket)) {
                    $this->log('Custom command has failed');
                    return false;
                }

                return true;
                break;
        }
    }

    /**
     * Handle custom worker commands
     *
     * @param $command
     * @param WorkerInterface $worker
     * @param $socket
     * @return bool|mixed
     */
    protected function onCustomWorkerCommand($command, WorkerInterface $worker, $socket)
    {
        $workerCommands = $worker->getCommands();

        // Check if Worker support command and call the specific worker command
        if (array_key_exists($command, $workerCommands)) {

            try {
                return call_user_func_array(array($worker, $workerCommands[$command]), array($command, $socket));
            } catch (\Exception $exp) {
                return false;
            }
        }

        return false;
    }

    /**
     * Handle the backend part
     *
     * @param ZMQSocket $socket
     * @return bool
     */
    protected function onFrontendCall(ZMQSocket $socket)
    {
        $clientAddr = $socket->recv();

        $this->emptyFrame($socket);

        $jobType = $socket->recv();

        $this->emptyFrame($socket);

        $params = $socket->recv();
        $params = json_decode($params, true);
        $params['job_type'] = $jobType;

        if (isset($this->jobTypes[$jobType])) {
            $job = $this->jobTypes[$jobType]($clientAddr);

            if (!$job instanceof JobInterface) {
                return;
            }

            if ($job instanceof LoggableInterface) {
                $job->setLogger($this->logger);
            }

            $job->setJobData($params);

            array_push($this->jobQueue, $job);
        }
    }

    protected function afterCall()
    {

        if (count($this->availableWorker) < 1) {
            return;
        }

        $pingCheck = new \DateTime('-10 sec');
        $pingTimeout = new \DateTime('-20 sec');
        $workerTimeout = new \DateTime('-5 min');
        $statusCheck = new \DateTime('-1 min');
        $resetStatusTime = false;

        /**
         * @var $worker \Zeichen32\Zmq\MessageBroker\Worker\WorkerInterface
         */
        foreach ($this->availableWorker as $worker) {

            // Handle close request
            if ($worker->hasCloseRequest() && !$worker->isWorking() && !$worker->isWaitingForNextPing()) {
                $worker->shutdown($this->backendSocket);
                $worker->setWorking();
            }

            // Check ping
            if (!$worker->isWorking() && !$worker->isWaitingForNextPing() && $pingCheck >= $worker->getLastPing()) {
                $worker->ping($this->backendSocket);
                $this->log('PING');
                continue;
            }

            // Request worker status
            if (!$worker->isWorking() && !$worker->isWaitingForNextPing() && $statusCheck >= $this->lastStatus && $worker->canReceiveStatusCommand()) {
                $worker->requestStatus($this->backendSocket);
                $resetStatusTime = true;
                continue;
            }

            // Check ping checkout
            if ($worker->isWaitingForNextPing() && $pingTimeout >= $worker->getLastPing()) {
                $this->onWorkerTimeout($worker);
                $this->log('Worker timed out...');
                continue;
            }

            // Worker has timeout
            if ($worker->isWorking() && $workerTimeout >= $worker->getLastPing()) {
                $this->onWorkerTimeout($worker);
                $this->log('Worker timed out...');
                continue;
            }

            // Try to execute worker jobs
            if (!$worker->isWorking() && !$worker->isWaitingForNextPing() && count($this->jobQueue) > 0) {

                /**
                 * @var $job \Zeichen32\Zmq\MessageBroker\Job\JobInterface
                 */
                foreach ($this->jobQueue as $key => $job) {

                    if (true == $worker->support($job)) {
                        if (!$job->isReady()) {
                            $job->addTTL();

                            if ($job->hasExceededTTL()) {
                                unset($this->jobQueue[$key]);
                                $this->log('Job ttl has exceeded...');
                                continue;
                            }
                        }

                        $worker->handleJob($this->backendSocket, $job);

                        unset($this->jobQueue[$key]);

                        $this->log('Send job to worker');
                        break;
                    }
                }
            }
        }

        if($statusCheck >= $this->lastStatus && $resetStatusTime === true) {
            $this->lastStatus = new \DateTime();
        }

    }

    protected function beforeCall(ZMQSocket $socket)
    {

    }

    /**
     * Reset Ping
     *
     * @param ZMQSocket $socket
     * @param WorkerInterface $worker
     * @return bool
     */
    protected function onPong(ZMQSocket $socket, WorkerInterface $worker)
    {
        $worker->setLastPing();
        $this->log('PONG');
        return true;
    }

    /**
     * Receive and check a empty frame
     *
     * @param $socket
     * @throws \LogicException
     */
    private function emptyFrame($socket)
    {
        $empty = $socket->recv();

        if (!empty($empty)) {
            throw new \LogicException('Expected empty frame, but receive none empty frame');
        }
    }

    protected function onSubscribeWorker($socket, $workerAddress)
    {
        $this->emptyFrame($socket);

        $workerType = $socket->recv();

        // Check worker type
        if (isset($this->workerTypes[$workerType])) {

            // Create new worker
            $worker = $this->workerTypes[$workerType]($workerAddress);

            if ($worker instanceof LoggableInterface) {
                $worker->setLogger($this->logger);
            }

            // Add worker to available worker list
            if ($worker instanceof WorkerInterface) {
                $this->availableWorker[$workerAddress] = $worker;
                $this->log('Worker has subscribte...');
                return true;
            }
        }

        throw new \LogicException('Worker cannot be created');
    }

    protected function onUnsubscribeWorker($socket, WorkerInterface $worker)
    {

        $this->removeJobFromWorker($worker);
        $worker->onUnsubscribe();

        $success = false;
        foreach ($this->availableWorker as $id => $workerEntry) {
            if ($worker->getWorkerAddress() == $workerEntry->getWorkerAddress()) {
                unset($this->availableWorker[$id]);
                $success = true;
            }
        }

        $this->log('Worker has unsubscribte...');
        return $success;
    }

    protected function onWorkerTimeout(WorkerInterface $worker)
    {

        $this->removeJobFromWorker($worker);

        $worker->onUnsubscribe();
        $workerAddress = $worker->getWorkerAddress();

        unset($this->availableWorker[$workerAddress]);
        unset($worker);

        $this->log('Worker has timeout...');
    }

    /**
     * Remove Job from worker
     *
     * @param WorkerInterface $worker
     */
    protected function removeJobFromWorker(WorkerInterface $worker)
    {

        if ($worker->hasJob()) {
            $job = $worker->getCurrentJob();
            $job->addTTL();

            if (!$job->hasExceededTTL()) {
                $this->jobQueue[] = $job;
                $worker->removeCurrentJob();

                $this->log('Job was added to queue again...');
            } else {
                $this->log('Job ttl was exceeded...');
            }
        }
    }
}
