<?php
/**
 * Created by PhpStorm.
 * User: jens
 * Date: 30.09.13
 * Time: 12:01
 */

namespace Zeichen32\Zmq\MessageBroker\Job;


interface JobFactoryInterface {
    /**
     * Return Job Type
     *
     * @return string
     */
    static public function getType();

    /**
     * Create a new Worker
     *
     * @param $clientAddress
     * @return JobInterface
     */
    public function create($clientAddress);

    /**
     * Create a new Job
     *
     * @param $clientAddress
     * @return JobInterface
     */
    function __invoke($clientAddress);
} 