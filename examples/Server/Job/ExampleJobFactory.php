<?php

class ExampleJobFactory implements \Zeichen32\Zmq\MessageBroker\Job\JobFactoryInterface {
    /**
     * Return Job Type
     *
     * @return string
     */
    static public function getType()
    {
        return 'EXAMPLE_JOB';
    }

    /**
     * Create a new Worker
     *
     * @param $clientAddress
     * @return \Zeichen32\Zmq\MessageBroker\Job\JobInterface
     */
    public function create($clientAddress)
    {
        return new ExampleJob($clientAddress);
    }

    /**
     * Create a new Job
     *
     * @param $clientAddress
     * @return \Zeichen32\Zmq\MessageBroker\Job\JobInterface
     */
    function __invoke($clientAddress)
    {
        return $this->create($clientAddress);
    }
}
